/*tugas 1*/
golden = () => {
   console.log("this is golden!!")
};
/*tugas 2*/
const newFunction = (firstName, lastName) => {
  return {
    fullName() {
      console.log(firstName + " " + lastName) 
    }
  }
};
/*tugas 3*/
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject;
/*tugas 4*/
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
/*tugas 5*/
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    ` incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`



console.log('1. Mengubah fungsi menjadi fungsi arrow\n');
golden();

console.log('\n2. Sederhanakan menjadi Object literal di ES6\n');
newFunction("William", "Imoh").fullName(); 

console.log('\n3. Destructuring\n');
console.log(firstName, lastName, destination, occupation);

console.log('\n4. Array Spreading\n');
console.log(combinedArray); 

console.log('\n5. Template Literals\n');
console.log(before);


