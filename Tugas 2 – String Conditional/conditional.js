console.log("1.  Tugas Conditional If-else");
var msg = '';
var nama = 'Esa Dwi Suryanta';
var peran = 'werewolf';
var daftar_peran = ["penyihir", "guard", "werewolf"];

if( nama == '' ){
	msg += "Nama harus diisi!";
}else if( peran == '' ){
	msg += 'Halo '+nama+', Pilih peranmu untuk memulai game!';
}else if(daftar_peran.includes(peran)){
  msg += "Selamat datang di Dunia Werewolf, " +nama+".\n";
  msg += "Halo "+peran+" "+nama+", ";
  if(peran.toLowerCase() == 'penyihir'){
    msg += "kamu dapat melihat siapa yang menjadi werewolf!";
  }else if(peran.toLowerCase() == 'guard'){
    msg += "kamu akan membantu melindungi temanmu dari serangan werewolf.";
  }else if(peran.toLowerCase() == 'werewolf'){
    msg += "Kamu akan memakan mangsa setiap malam!";
  }
}else{
  msg += "Maaf "+nama+" peran tidak terdaftar!";
}
console.log(msg);


console.log("2. Tugas Conditional Switch Case");
var msg = '';
var hari = 01; 
var bulan = 12; 
var tahun = 2200;
var nama_bulan = '';

switch(bulan){
	case 1: nama_bulan = 'Januari'; break;
	case 2: nama_bulan = 'Februari';break;
	case 3: nama_bulan = 'Maret'; break;
	case 4: nama_bulan = 'April'; break;
	case 5: nama_bulan = 'Mei'; break;
	case 6: nama_bulan = 'Juni'; break;
	case 7: nama_bulan = 'Juli'; break;
	case 8: nama_bulan = 'Agustus'; break;
	case 9: nama_bulan = 'September'; break;
	case 10: nama_bulan = 'Oktober'; break;
	case 11: nama_bulan = 'November'; break;
	case 12: nama_bulan = 'Desember'; break;
}

if( hari == 0 || hari > 31 ){
  msg = "Tanggal tidak valid, silahkan masukkan angka 1 s/d 31.";
}else if( bulan == 0 || bulan > 12 ){
  msg = "Bulan tidak valid, silahkan masukkan angka 1 s/d 12.";
}else if( tahun < 1900 || tahun > 2200){
  msg = "Tahun tidak valid, silahkan masukkan angka 1900 s/d 2200.";
}else{
  msg = hari+' '+nama_bulan+' '+tahun;
}
console.log(msg);