function AscendingTen(num) {
  var max = num+10;
  var data = '';
  if(typeof num === 'undefined'){
    data += '-1';
  }else{
    while (num < max) {
      data += num+' ';
      num++;
    }
  }
  return data;
}

function DescendingTen(num) {
  var min = num-10;
  var data = '';
  if(typeof num === 'undefined'){
    data += '-1';
  }else{
    while (min < num) {
      data += num+' ';
      num--;
    }
  }
  return data;
}

function ConditionalAscDesc(reference, check) {
  var data = '';
  if(typeof reference === 'undefined' || typeof check === 'undefined'){
    data = -1;
  }else{
      if(check%2 === 0){
        data = DescendingTen(reference);
      }else{
        data = AscendingTen(reference);
      }
  }
  return data;
}

function ularTangga() {
  let text = ""
  let awal = 100
  let tangga = 1
  let counter = 1
  while (awal > 0) {
    if (counter > 10){
      counter = 1
      text += "\n"
      tangga++
      awal = awal - 10
    }
    if (counter != 1){
      if (tangga % 2 == 0) {
        awal++
      } else {
        awal--
      }
    }
    if (awal > 0) { text += awal + " "}
    counter++
  }
  return text
}

console.log('\nTEST CASES Ascending Ten\n');
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log('\nTEST CASES Descending Ten\n');
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log('\nTEST CASES Conditional Ascending Descending\n');
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

console.log('\n// TEST CASE Ular Tangga\n');
console.log(ularTangga()) 