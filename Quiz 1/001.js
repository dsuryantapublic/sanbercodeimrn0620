function balikString(string) {
  var reserve = '';
  var i = string.length;
    while (i > 0) {
      i--;
      reserve += string[i];
    }
  return reserve;
}

function palindrome(string) {
  var reserve = balikString(string);
  if(reserve === string){
    return true;
  }else{
    return false;
  }
}

function bandingkan(num1, num2) {
  var nilai = '';
  if((typeof num1 === 'undefined' && typeof num2 === 'undefined') ||
    num1 === num2 || (num1 < 0 || num2 < 0)){
    nilai = -1;
  }else if(num1 > num2){
    nilai = num1;
  }else if(num1 < num2){
    nilai = num2;
  }else{
    nilai = num1;
  }
  return nilai;
}

console.log('TEST CASES BalikString\n');
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log('\nTEST CASES Palindrome\n');
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

console.log('\nTEST CASES Bandingkan Angka\n');
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
