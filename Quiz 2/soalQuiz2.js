const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
];

/*soal 1*/
class Score {
    render(subject, points, email){
    	var nilai = {
    		email:email, 
    		subject:subject,
    		points:points
    	};
    	if(Array.isArray(points)){
    		nilai.avg = this.avg(points);	
    	}
    	console.log(nilai);
    }
    avg(points){
    	var total = 0;
    	for (var i = 0; i < points.length; i++) {
    		total += points[i];
    	}
		return (total/points.length);
    }
}
/*soal 2*/
function viewScores(data, subject) {
	let idx = data[0].indexOf(subject);
	var person = data.slice(1);
	for (var i = 0; i < person.length; i++) {
    	score.render(subject, person[i][idx], person[i][0]);		
    }
}
/*soal 3*/
function recapScores(data) {
	var person = data.slice(1);
	var output = '';
	for (var x = 0; x < person.length; x++) {
		let [email, ...points] = person[x];
		var avg = score.avg(points);
		output += 'Email: '+email+'\n';
		output += 'Rata-rata: '+avg+'\n';
		if(avg > 90){
			output += 'Predikat: honour\n';
		}else if(avg > 80){
			output += 'Predikat: graduate\n';
		}else if(avg > 70){
			output += 'Predikat: participant\n';
		}else{
			output += 'Predikat: \n';
		}
		output += '\n';
	}
	console.log(output);
}

console.log('SOAL CLASS SCORE\n');
var point = [100, 80, 90];
var score = new Score();
score.render("Quiz 2", point, "dsuryantapublic@gmail.com");

console.log('\nSOAL CREATE SCORE\n');
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

console.log('\nSOAL RECAP SCORE\n');
recapScores(data);