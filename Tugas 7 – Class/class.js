class Animal{
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
class Ape extends Animal {
    yell(){
      console.log("Auooooo!");
    }
}
class Frog extends Animal {
    jump(){
      console.log("hop hop");
    }
}

var templates, timer; 

class Clock {
    constructor(obj) {
      templates = obj.template;
    }
    render(){
      var date = new Date();
      var hours = date.getHours();
      var mins = date.getMinutes();
      var secs = date.getSeconds();

      if (hours < 10) hours = '0' + hours;
      if (mins < 10) mins = '0' + mins;
      if (secs < 10) secs = '0' + secs;
      console.log(templates.replace('h', hours).replace('m', mins).replace('s', secs));
    }
    stop(){
      clearInterval(timer);
    }
    start(){
      this.render();
      timer = setInterval(this.render, 1000);
    }
}



console.log("1. Animal Class\n");

console.log('Release 0');
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\nRelease 1');
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

console.log('\n2. Function to Class\n');
var clock = new Clock({template: 'h:m:s'});
clock.start(); 
//clock.stop();  












