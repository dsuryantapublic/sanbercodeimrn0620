var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
var readBooks = require('./callback.js');

function recursive(waktu,obj_buku,i=0){
	if (obj_buku.length > i) {
		readBooks(waktu,obj_buku[i], function(sisa_waktu){
			if (sisa_waktu > 0) {
				recursive(sisa_waktu,obj_buku,i+1);
			}
		})
	}
}

recursive(10000,books);