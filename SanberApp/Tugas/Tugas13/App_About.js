'use strict';

import React, { Component } from 'react';
import { Entypo } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons'; 

import {
	StyleSheet,
	View,
	Text,
	Image,
	TextInput,
	TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

class App_About extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.body}>
					<View style={{alignItems:'center'}}>
						<Text style={styles.pageTitle}>Tentang Saya</Text>
					</View>
					<View style={styles.profilPic}>
						<Image source={{uri:'https://1.gravatar.com/avatar/79adbe15382faf729293028ebbe06a9a?s=200&d=mm&r=g'}} style={{width:200,height:200,borderRadius:100}} />
					</View>
					<View style={{alignItems:'center'}}>
						<Text style={{color:'#003366',fontSize:25,fontWeight:'bold'}}>Esa Dwi Suryanta</Text>
					</View>
					<View style={{alignItems:'center'}}>
						<Text style={{color:'#3EC6FF',fontSize:18,fontWeight:'bold'}}>React Native Developer</Text>
					</View>


					

					<View style={styles.project}>
						<Text style={{fontSize:15, marginBottom:0, fontWeight:'bold'}}>Project</Text>
						<View style={styles.projectIcon}>
							<Fontisto name="gitlab" size={40} color="black" />
							<Text style={{color:'#003366',fontSize:12,fontWeight:'bold'}}>Esa Dwi Suryanta</Text>
							<AntDesign name="github" size={40} color="black" />
							<Text style={{color:'#003366',fontSize:12,fontWeight:'bold'}}>@esadwisuryanta</Text>
						</View>
					</View>


					<View style={styles.contact}>
						<Text style={{fontSize:15, marginBottom:10, fontWeight:'bold'}}>Kontak Saya</Text>
						<View style={styles.contactIcon}>
							<Entypo name="facebook-with-circle" size={40} color="#003366" />
							<Text style={{color:'#003366',fontSize:12,fontWeight:'bold'}}> Esa Dwi Suryanta</Text>
						</View>
						<View style={styles.contactIcon}>
							<Entypo name="instagram" size={40} color="black" />
							<Text style={{color:'#003366',fontSize:12,fontWeight:'bold'}}> @esadwisuryanta</Text>
						</View>
						<View style={styles.contactIcon}>
							<Entypo name="twitter-with-circle" size={40} color="#003366" />
							<Text style={{color:'#003366',fontSize:12,fontWeight:'bold'}}> @esadwisuryanta</Text>
						</View>
					</View>
				</View>
			</View>
		);
	}
}



<View style={styles.boxMenu.push({width: 130})}>
						<Text>Libary / Framework</Text>
					</View>




const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	body: {
		paddingTop: 50
	},
	profilPic: {
	    alignItems: 'center',
	    padding: 15
	},
	pageTitle: {
		fontSize: 35,
		fontWeight: 'bold',
		color: '#003366'
	},
	contact: {
		backgroundColor:'#EFEFEF',
		alignItems: 'center',
		padding: 25,
		margin: 5
	},
	contactIcon: {
		flex: 1, 
		flexDirection: 'row',
		alignItems: 'center',
	},
	project: {
		backgroundColor:'#EFEFEF',
		alignItems: 'center',
		padding: 5,
		margin: 5
	},
	projectIcon: {
		flex: 1, 
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor:'#EFEFEF',
		margin: 5
	},
});

export default App_About;