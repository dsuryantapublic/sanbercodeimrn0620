'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	View,
	Text,
	Image,
	TextInput,
	TouchableOpacity
} from 'react-native';

class App_Login extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={{marginTop: 59}}>
					<Image source={require('./images/logo.png')} style={{height:102}} />
				</View>
				<View style={styles.pageTitleContainer}>
					<Text style={styles.pageTitle}>Login</Text>
				</View>
				<View style={styles.body}>
					<View style={styles.form}>
						<Text style={styles.label}>Username / Email</Text>
						<TextInput style={styles.input} />
					</View>
					<View style={styles.form}>
						<Text style={styles.label}>Password</Text>
						<TextInput style={styles.input} />
					</View>
					<View style={styles.aksi}>
						<TouchableOpacity style={styles.tombolTerang}>
							<Text style={{color:'white', fontSize:24}}>Masuk</Text>
						</TouchableOpacity>
						<Text style={{color:'#3EC6FF', fontSize:20}}>atau</Text>
						<TouchableOpacity style={styles.tombolGelap} >
							<Text style={{color:'white', fontSize:24}}>Daftar ?</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	header: {
		marginTop: 60,
	},
	body: {
		paddingTop: 25
	},
	pageTitleContainer: {
	    alignItems: 'center'
	},
	pageTitle: {
		fontSize: 24,
		marginTop: 50,
		color: '#003366'
	},
	form: {
		paddingHorizontal: 30,
		marginBottom: 10
	},
	label: {
		fontSize: 18,
		color: '#003366'
	},
	input: {
		padding: 10,
		height: 48,
		borderColor: '#003366',
		borderWidth: 1
	},
	aksi: {
		marginTop: 30,
		height: 130,
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	tombolGelap: {
		borderRadius: 16,
		height: 40,
		backgroundColor: '#003366',
		width: 140,
		alignItems: 'center',
		justifyContent: 'center'
	},
	tombolTerang: {
		borderRadius: 16,
		height: 40,
		backgroundColor: '#3EC6FF',
		width: 140,
		alignItems: 'center',
		justifyContent: 'center'
	}
});

export default App_Login;