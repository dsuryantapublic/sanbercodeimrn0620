import React from 'react';
import { View, Text, FlatList, StyleSheet, Image } from 'react-native';
import SkillItem from './components/SkillItem';
import skillData from './skillData.json';

export default class SkillScreen extends React.Component {

  render() {

    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Image source={require('../Tugas13/images/logo.png')} style={{height:51,width:187.5}} />
        </View>

        <View style={styles.profilContainer}>
          <View style={{justifyContent:'center',height:40}}>
            <Image source={{uri:'https://1.gravatar.com/avatar/79adbe15382faf729293028ebbe06a9a?s=200&d=mm&r=g'}} style={{width:30,height:30,borderRadius:15}} />
          </View>
          <View style={styles.profilDetails}>
            <Text style={styles.profilHai}>Hai,</Text>
            <Text style={styles.profilName}>Nova Arief</Text>
          </View>
        </View>

        <View style={{borderBottomWidth:5,borderBottomColor:'#3EC6FF'}}>
          <Text style={{fontSize:30,color:'#003366'}}>SKILL</Text>
        </View>

        <View style={styles.boxKategori}>
          <View style={styles.textKategoriContainer}>
            <Text style={styles.textKategori}>Library / Framework</Text>
          </View>
          <View style={styles.textKategoriContainer}>
            <Text style={styles.textKategori}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.textKategoriContainer}>
            <Text style={styles.textKategori}>Teknologi</Text>
          </View>
        </View>

        <FlatList 
            data={skillData.items}
            renderItem={(skill)=><SkillItem skill={skill.item}/>}
            keyExtractor={(item)=>item.id}
            ItemSeparatorComponent={()=><View style={{height:10}} />}
          />

      </View>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  profilContainer: {
    flexDirection: 'row',
    justifyContent:'center'
  },
  profilDetails: {
    paddingHorizontal: 15,
    flex: 1
  },
  profilHai: {
    fontSize: 16
  },
  profilName: {
    flex: 1,
    fontWeight: 'bold',
    color: '#003366'
  },
  boxKategori: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 10
  },
  textKategoriContainer: {
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor:'#B4E9FF',
    borderRadius:10
  },
  textKategori: {
    fontWeight: 'bold',
    color:'#003366'
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  }
});

