var nama =  [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]];
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
var produk = [
    {namaBarang:'Sepatu Stacattu', hargaBarang: 1500000},
    {namaBarang:'Baju Zoro', hargaBarang: 500000},
    {namaBarang:'Baju H&N', hargaBarang: 250000},
    {namaBarang:'Sweater Uniklooh', hargaBarang: 175000},
    {namaBarang:'Casing Handphone', hargaBarang: 50000}
    ];

function arrayToObject(inputArray){
  var output = {};
  var tahun = new Date().getFullYear();

  if( inputArray.length == 0 ){
    output = [];
  }else{
    for( var i = 0; i<inputArray.length; i++ ){
      if( typeof inputArray[i][3] != 'undefined' && Number.isInteger(inputArray[i][3]) )
      {
        if( inputArray[i][3] < tahun  ){
          var age = (tahun-inputArray[i][3]);
        }else{
          var age = 'Invalid Birth Year';
        }
      }else{
        var age = 'Invalid Birth Year';
      }
      
      var fullName = inputArray[i][0]+' '+inputArray[i][1];
      output[fullName] = { firstName: inputArray[i][0], lastName: inputArray[i][1], gender: inputArray[i][2], age: age}
    }
  }
  console.log(output);
}

function shoppingTime(memberId, money) {
  var output = '';
  if(memberId === '' || typeof memberId == 'undefined'){
    output = 'Mohon maaf, toko X hanya berlaku untuk member saja';
  }else if( typeof money == 'undefined' || parseInt(money) < 50000){
    output = 'Mohon maaf, uang tidak cukup';
  }else{
      var sliceProduk = produk.slice(0);
      sliceProduk.sort(function(a,b) {
          return b.hargaBarang - a.hargaBarang;
      });

      var barangDibeli = [];
      var totalBelanja = 0;
      var kembalian = 0;
      var jumlahUang = money;

      for( var i=0; i<sliceProduk.length;i++){
        if( jumlahUang >= sliceProduk[i].hargaBarang ){
          totalBelanja += sliceProduk[i].hargaBarang;
          barangDibeli.push(sliceProduk[i].namaBarang);
          jumlahUang = (jumlahUang-sliceProduk[i].hargaBarang);
        }
      }

      output = {memberId:memberId, money:money,listPurchased:barangDibeli,changeMoney:jumlahUang}
  }
  return output;
}

function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var rangeRute = [];
  var output = [];
  
  if(typeof arrPenumpang !== 'undefined'){
    for(var i=0; i<rute.length;i++){
      rangeRute[rute[i]] = i;
    }  
    for(var x = 0; x<arrPenumpang.length;x++){ 
      var harga = 0;
      for(var j=rangeRute[arrPenumpang[x][1]];j<rangeRute[arrPenumpang[x][2]];j++){
        harga += 2000;
      }
      output[x] = {penumpang:arrPenumpang[x][0],naikDari:arrPenumpang[x][1],tujuan:arrPenumpang[x][2],bayar:harga}
    }
  }
  return output;
}

console.log('Soal No. 1 (Array to Object)\n');
arrayToObject(people);
console.log('Error case \n');
arrayToObject([]);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

console.log('\nSoal No. 2 (Shopping Time)\n');
console.log(shoppingTime('1820RzKrnWn08', 2475000));
/*{ 
    memberId: '1820RzKrnWn08',
    money: 2475000,
    listPurchased:
      [ 'Sepatu Stacattu',
        'Baju Zoro',
        'Baju H&N',
        'Sweater Uniklooh',
        'Casing Handphone' ],
    changeMoney: 0 
  }*/
console.log(shoppingTime('82Ku8Ma742', 170000));
/*{ 
  memberId: '82Ku8Ma742',
  money: 170000,
  listPurchased:
    ['Casing Handphone'],
  changeMoney: 120000 
 }
 */
console.log(shoppingTime('', 2475000)); 
/*Mohon maaf, toko X hanya berlaku untuk member saja*/
console.log(shoppingTime('234JdhweRxa53', 15000)); 
/*Mohon maaf, uang tidak cukup*/
console.log(shoppingTime()); 
/*Mohon maaf, toko X hanya berlaku untuk member saja*/

console.log('\nSoal No. 3 (Naik Angkot)\n');
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
/*
  [{ penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 }]
*/
console.log(naikAngkot([])); 
/*[]*/