console.log('TUGAS 1.1 LOOPING WHILE\n');

var i = 2;
while (i <= 20) {
  if(i%2 === 0){
     console.log(i+' - I love coding');
  }
  i++
}

console.log('\nTUGAS 1.2 LOOPING WHILE\n');

var i = 20;
while (i > 0) {
  if(i%2 === 0){
     console.log(i+' - I will become a mobile developer');
  }
  i--;
}

console.log('\nTUGAS 2. LOOPING FOR\n');

for (i = 1; i <= 20; i++) {
  if(i%2 === 0){
    console.log(i+' - Berkualitas');
  }else{
    if(i%3 === 0){
      console.log(i+' - I Love Coding');
    }else{
      console.log(i+' - Santai');
    }
  }
}

console.log('\nTUGAS 3. PERSEGI PANJANG\n');

for ( i = 1; i <= 4; i++) {
  var str = '';
    for ( j = 1; j <= 8; j++) {
      str += '#';
    }
    console.log(str);
}

console.log('\nTUGAS 4. MEMBUAT TANGGA\n');

for (i = 1; i <= 7; i++) {
  var str = '';
  for (j = 1; j <= i; j++) {
    str += '#';
  }
  console.log(str);
}

console.log('\nTUGAS 5. MEMBUAT PAPAN CATUR\n');

for ( i = 1; i <= 8; i++) {
  var str = '';
    for ( j = 1; j <= 8; j++) {
      if(i%2 === 0){
        str += ((j%2 === 0) ? ' ' : '#');
      }else{
        str += ((j%2 === 0) ? '#' : ' ');
      }
      
    }
    console.log(str);
}