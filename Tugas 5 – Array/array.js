var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]; 
var stringInput = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];            
function range(start, finish) {
    var data = [];
    if(typeof start === 'undefined' || typeof finish === 'undefined'){
      data.push(-1);
    }else{
      if(start < finish){
        while (start <= finish) {
          data.push(start);
          start++;
        }
      }else{
        while (finish <= start) {
          data.push(start);
          start--;
        }
      }
    }
    return data
};

function rangeWithStep(start, finish, step) {
    var data = [];
    if(typeof start === 'undefined' || typeof finish === 'undefined'){
      data.push(-1);
    }else{
      if(start < finish){
        while (start <= finish) {
          data.push(start);
          start = start + step;
        }
      }else{
        while (finish <= start) {
          data.push(start);
          start = start - step;
        }
      }
    }
    return data
};

function sum(start, finish, step) {
    var sum = 0;
    var data = [];
    if(typeof start === 'undefined'){
      sum = 0;
    }else if(typeof finish === 'undefined'){
      sum = 1;
    }else{
      if(typeof step === 'undefined'){
        step = 1;
      }
      var i = 0;
      data = rangeWithStep(start, finish, step);
      while (i < data.length) {
        sum += data[i];
        i++;
      }
    }
   return sum;
};

function dataHandling(inputArray){
  var data = '';
  for(var i = 0; i<inputArray.length; i++){
    data += 'Nomor ID : ' + inputArray[i][0] + "\n";
    data += 'Nama Lengkap : ' + inputArray[i][1] + "\n";
    data += 'TTL : ' + inputArray[i][2] +", "+ inputArray[i][3] +"\n";
    data += 'Hobi : ' + inputArray[i][4] + "\n\n";
  }
  return data;
}

function balikKata(stringInput){
  var arrString = [];
  for(var i=0;i<stringInput.length;i++){
    arrString.push(stringInput[i]);
  }
  arrString.reverse();
  return arrString.join("");
}

function dataHandling2(strInput){
  strInput.splice(1,4,strInput[1].trim()+" Elsharawy","Provinsi "+strInput[2],strInput[3],"Pria","SMA Internasional Metro");
  console.log(strInput);
  
  var bulan = strInput[3].split("/");
	switch(bulan[1]){
		case '01': namaBulan = "Januari"; break;
		case '02': namaBulan = "Februari"; break;
		case '03': namaBulan = "Maret"; break;
		case '04': namaBulan = "April"; break;
		case '05': namaBulan = "Mei"; break;
		case '06': namaBulan = "Juni"; break;
		case '07': namaBulan = "Juli"; break;
		case '08': namaBulan = "Agustus"; break;
		case '09': namaBulan = "September"; break;
		case '10': namaBulan = "Oktober"; break;
		case '11': namaBulan = "November"; break;
		case '12': namaBulan = "Desember"; break;
	}
  console.log(namaBulan);

  var splitBulan = bulan.slice()
	splitBulan.sort(function(val1, val2){return val2-val1});
  console.log(splitBulan);

  var formatBulan = bulan.join('-');
	console.log(formatBulan)

	var formatNama = String(strInput[1]).slice(0,15)
	console.log(formatNama)
}

console.log('Soal No. 1 (Range)\n');
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log('\nSoal No. 2 (Range with Step)\n');
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('\nSoal No. 3 (Sum of Range)\n');
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log('\nSoal No. 4 (Array Multidimensi)\n');
console.log(dataHandling(input));

console.log('\nSoal No. 5 (Balik Kata)\n');
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log('\nSoal No. 6 (Metode Array)\n');
dataHandling2(stringInput);